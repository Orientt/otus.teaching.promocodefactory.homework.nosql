﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoDbInitializer : IDbInitializer
    {
        private readonly IMongoCollection<Employee> _emploeeCollection;
        private readonly IMongoCollection<Role> _roleCollection;

        public MongoDbInitializer(IMongoCollection<Employee> emploeeCollection, IMongoCollection<Role> roleCollection)
        {
            _emploeeCollection = emploeeCollection;
            _roleCollection = roleCollection;
        }


        public void InitializeDb()
        {
            _emploeeCollection.InsertMany(FakeDataFactory.Employees);
            _roleCollection.InsertMany(FakeDataFactory.Roles);
        }
    }
}